! 
!  Copyright (C) 2015  CSC - IT Center for Science Ltd.
!
!  Licensed under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  Code is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  Copy of the GNU General Public License can be onbtained from
!  see <http://www.gnu.org/licenses/>.
! 

program exer1
  implicit none
  integer :: var1, var2
  var1 = 1
  var2 = 2

  !$OMP PARALLEL PRIVATE(VAR1, VAR2)
  print *, 'Region 1:       var1=', var1, 'var2=', var2
  var1 = var1 + 1
  var2 = var2 + 1
  !$OMP END PARALLEL
  print *, 'After region 1: var1=', var1, 'var2=', var2
  print *

  !$OMP PARALLEL FIRSTPRIVATE(VAR1, VAR2)
  print *, 'Region 2:       var1=', var1, 'var2=', var2
  var1 = var1 + 1
  var2 = var2 + 1
  !$OMP END PARALLEL
  print *, 'After region 2: var1=', var1, 'var2=', var2
  print *

  !$OMP PARALLEL
  print *, 'Region 3:       var1=', var1, 'var2=', var2
  var1 = var1 + 1
  var2 = var2 + 1
  !$OMP END PARALLEL
  print *, 'After region 3: var1=', var1, 'var2=', var2
  print *

end program exer1
