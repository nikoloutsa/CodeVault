#ifndef MPICOMM_H
#define MPICOMM_H

#include "configuration.h"
#include "simulation.h"

void mpi_broadcast_configuration(conf_t *configuration);

void mpi_reduce_sim_info(const sim_info_t *local_info, sim_info_t *info);

void mpi_communicate_particles_dynamic(sim_t *s);

void mpi_communicate_particles_rma(sim_t *s);

void mpi_communicate_particles_collective(sim_t *s);

#endif
