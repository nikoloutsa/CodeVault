#pragma once

#include <cstddef>
#include <getopt.h>

#include <boost/program_options.hpp>
#include <boost/serialization/serialization.hpp>

struct Configuration {
	size_t NoIterations{1000};
	size_t NoParticles{1000};

	template <class Archive> void serialize(Archive& ar, const unsigned int version) {
		ar& NoIterations;
		ar& NoParticles;
	}

  private:
	friend class boost::serialization::access;
};
BOOST_IS_MPI_DATATYPE(Configuration) // performance hint allowed for contiguous data

Configuration parseArgs(int argc, char* argv[]) {
	namespace po = boost::program_options;
	size_t noParticles{1000};
	size_t noIterations{1000};
	po::options_description desc{"Allowed options"};
	desc.add_options()("help,h", "produce help message")("particles,p", po::value<size_t>(), "number of particles")(
	    "iterations,i", po::value<size_t>(), "number of iterations");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	if (vm.count("help")) std::cout << desc << '\n';
	if (vm.count("particles")) noParticles = vm["particles"].as<size_t>();
	if (vm.count("iterations")) noIterations = vm["iterations"].as<size_t>();

	return {noIterations, noParticles};
}
