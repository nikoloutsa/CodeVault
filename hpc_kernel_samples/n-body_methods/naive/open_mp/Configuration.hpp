#pragma once

#include <cstddef>
#include <getopt.h>

struct Configuration {
	std::size_t NoIterations;
	std::size_t NoParticles;
};

Configuration parseArgs(int argc, char* argv[]) {
	int option;
	std::size_t noParticles{1000};
	std::size_t noIterations{1000};
	while ((option = getopt(argc, argv, "p:i:")) >= 0) {
		switch (option) {
		case 'p':
			noParticles = std::atoi(optarg);
			break;
		case 'i':
			noIterations = std::atoi(optarg);
			break;
		}
	}
	return {noIterations, noParticles};
}
