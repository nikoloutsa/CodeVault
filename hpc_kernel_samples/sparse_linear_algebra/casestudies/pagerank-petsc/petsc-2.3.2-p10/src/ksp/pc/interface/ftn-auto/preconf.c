#include "petsc.h"
#include "petscfix.h"
/* precon.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscksp.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcdestroy_ PCDESTROY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcdestroy_ pcdestroy
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcdiagonalscaleset_ PCDIAGONALSCALESET
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcdiagonalscaleset_ pcdiagonalscaleset
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pccreate_ PCCREATE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pccreate_ pccreate
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapply_ PCAPPLY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapply_ pcapply
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplysymmetricleft_ PCAPPLYSYMMETRICLEFT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplysymmetricleft_ pcapplysymmetricleft
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplysymmetricright_ PCAPPLYSYMMETRICRIGHT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplysymmetricright_ pcapplysymmetricright
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplytranspose_ PCAPPLYTRANSPOSE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplytranspose_ pcapplytranspose
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pchasapplytranspose_ PCHASAPPLYTRANSPOSE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pchasapplytranspose_ pchasapplytranspose
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplybaorab_ PCAPPLYBAORAB
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplybaorab_ pcapplybaorab
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplybaorabtranspose_ PCAPPLYBAORABTRANSPOSE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplybaorabtranspose_ pcapplybaorabtranspose
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplyrichardsonexists_ PCAPPLYRICHARDSONEXISTS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplyrichardsonexists_ pcapplyrichardsonexists
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcapplyrichardson_ PCAPPLYRICHARDSON
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcapplyrichardson_ pcapplyrichardson
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcsetup_ PCSETUP
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcsetup_ pcsetup
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcsetuponblocks_ PCSETUPONBLOCKS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcsetuponblocks_ pcsetuponblocks
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcsetoperators_ PCSETOPERATORS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcsetoperators_ pcsetoperators
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcgetfactoredmatrix_ PCGETFACTOREDMATRIX
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcgetfactoredmatrix_ pcgetfactoredmatrix
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcpresolve_ PCPRESOLVE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcpresolve_ pcpresolve
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcpostsolve_ PCPOSTSOLVE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcpostsolve_ pcpostsolve
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pccomputeexplicitoperator_ PCCOMPUTEEXPLICITOPERATOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pccomputeexplicitoperator_ pccomputeexplicitoperator
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   pcdestroy_(PC pc, int *__ierr ){
*__ierr = PCDestroy(
	(PC)PetscToPointer((pc) ));
}
void PETSC_STDCALL   pcdiagonalscaleset_(PC pc,Vec s, int *__ierr ){
*__ierr = PCDiagonalScaleSet(
	(PC)PetscToPointer((pc) ),
	(Vec)PetscToPointer((s) ));
}
void PETSC_STDCALL   pccreate_(MPI_Fint * comm,PC *newpc, int *__ierr ){
*__ierr = PCCreate(
	MPI_Comm_f2c( *(comm) ),newpc);
}
void PETSC_STDCALL   pcapply_(PC pc,Vec x,Vec y, int *__ierr ){
*__ierr = PCApply(
	(PC)PetscToPointer((pc) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   pcapplysymmetricleft_(PC pc,Vec x,Vec y, int *__ierr ){
*__ierr = PCApplySymmetricLeft(
	(PC)PetscToPointer((pc) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   pcapplysymmetricright_(PC pc,Vec x,Vec y, int *__ierr ){
*__ierr = PCApplySymmetricRight(
	(PC)PetscToPointer((pc) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   pcapplytranspose_(PC pc,Vec x,Vec y, int *__ierr ){
*__ierr = PCApplyTranspose(
	(PC)PetscToPointer((pc) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   pchasapplytranspose_(PC pc,PetscTruth *flg, int *__ierr ){
*__ierr = PCHasApplyTranspose(
	(PC)PetscToPointer((pc) ),flg);
}
void PETSC_STDCALL   pcapplybaorab_(PC pc,PCSide *side,Vec x,Vec y,Vec work, int *__ierr ){
*__ierr = PCApplyBAorAB(
	(PC)PetscToPointer((pc) ),*side,
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ),
	(Vec)PetscToPointer((work) ));
}
void PETSC_STDCALL   pcapplybaorabtranspose_(PC pc,PCSide *side,Vec x,Vec y,Vec work, int *__ierr ){
*__ierr = PCApplyBAorABTranspose(
	(PC)PetscToPointer((pc) ),*side,
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ),
	(Vec)PetscToPointer((work) ));
}
void PETSC_STDCALL   pcapplyrichardsonexists_(PC pc,PetscTruth *exists, int *__ierr ){
*__ierr = PCApplyRichardsonExists(
	(PC)PetscToPointer((pc) ),exists);
}
void PETSC_STDCALL   pcapplyrichardson_(PC pc,Vec x,Vec y,Vec w,PetscReal *rtol,PetscReal *abstol,PetscReal *dtol,PetscInt *its, int *__ierr ){
*__ierr = PCApplyRichardson(
	(PC)PetscToPointer((pc) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ),
	(Vec)PetscToPointer((w) ),*rtol,*abstol,*dtol,*its);
}
void PETSC_STDCALL   pcsetup_(PC pc, int *__ierr ){
*__ierr = PCSetUp(
	(PC)PetscToPointer((pc) ));
}
void PETSC_STDCALL   pcsetuponblocks_(PC pc, int *__ierr ){
*__ierr = PCSetUpOnBlocks(
	(PC)PetscToPointer((pc) ));
}
void PETSC_STDCALL   pcsetoperators_(PC pc,Mat Amat,Mat Pmat,MatStructure *flag, int *__ierr ){
*__ierr = PCSetOperators(
	(PC)PetscToPointer((pc) ),
	(Mat)PetscToPointer((Amat) ),
	(Mat)PetscToPointer((Pmat) ),*flag);
}
void PETSC_STDCALL   pcgetfactoredmatrix_(PC pc,Mat *mat, int *__ierr ){
*__ierr = PCGetFactoredMatrix(
	(PC)PetscToPointer((pc) ),mat);
}
void PETSC_STDCALL   pcpresolve_(PC pc,KSP ksp, int *__ierr ){
*__ierr = PCPreSolve(
	(PC)PetscToPointer((pc) ),
	(KSP)PetscToPointer((ksp) ));
}
void PETSC_STDCALL   pcpostsolve_(PC pc,KSP ksp, int *__ierr ){
*__ierr = PCPostSolve(
	(PC)PetscToPointer((pc) ),
	(KSP)PetscToPointer((ksp) ));
}
void PETSC_STDCALL   pccomputeexplicitoperator_(PC pc,Mat *mat, int *__ierr ){
*__ierr = PCComputeExplicitOperator(
	(PC)PetscToPointer((pc) ),mat);
}
#if defined(__cplusplus)
}
#endif
