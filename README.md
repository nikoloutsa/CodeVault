PRACE CodeVault
===============
PRACE CodeVault is an open repository containing various high
performance computing code samples. The project aims to support
self-learning of HPC programming and will be used as an Open platform
for the HPC community to share example code snippets, proof-of-concept
codes and so forth.  

CodeVault contains training material from PRACE partners, as well as
example codes of common HPC kernels such as dense and sparse linear
algebra, spectral and N-body methods, structured and unstructured
grids, Monte Carlo methods and parallel I/O. The code samples are
published as open source and can be used both for educational purposes
and as parts of real application suites (as permitted by particular
license).  

How to contribute
-----------------
Any contributions (new code samples, bug fixes, improvements etc.) are
warmly welcome. In order to contribute, please follow the standard
Gitlab workflow:

1. Fork the project into your personal space on GitLab.com
2. Create a feature branch
3. Work on your contributions
4. Push the commit(s) to your fork
5. Submit a merge request to the master branch

Instructions for cloning/building a single example
-----------

__[Cloning a single example]:__

Since version 1.7 git supports sparse checkouts, thus users can only clone specific folders and not the whole repository structure.
In order to do this we provide an example for cloning the GEMM/cuBLAS example. The steps are:

```bash
git init gemm_cublas
cd gemm_cublas
git config core.sparsecheckout true
echo "hpc_kernel_samples/dense_linear_algebra/gemm/cublas">> .git/info/sparse-checkout
git checkout master
```

The example code will be placed in gemm_cublas/hpc_kernel_samples/dense_linear_algebra/gemm/cublas/

__[Building a single example]:__

In order to build the example we cloned above, we follow the typical CMake steps:

```bash
cd gemm_cublas/hpc_kernel_samples/dense_linear_algebra/gemm/cublas
mkdir build
cd build
cmake ..
make
make install # optional
```

This will compile the example provided that CUDA is found.

